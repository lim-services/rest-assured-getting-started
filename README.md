# Rest Assured Getting Started
This repository is about learning the basic concept of writing
rest assured api. It also shows how to do chaining concept, how to do
assertion, extract response and how to use hamcrest.

### Hamcrest Cheatsheet

| Methods | Descriptions  |
| :-: | :-: |
| hasItem()| Checks single element in a collection |
| not(hasItem()) | Check single element is NOT in a collection|
| hasItems() | Check all elements are in a collection|
| contains() | Check all elements are in a collection and in a strict order|
| containsInAnyOrder() | Check all elements are in a collection and in any order|
| empty() | Check if collection is empty|
| not(emptyArray())| Check if the Array is not empty|
| hasSize() | Check size of a collection|
| everyItem(startsWith()) | Check if every item in a collection starts with specified string|
| hasKey() | Check if Map has the specified key [value is not checked]|
| hasValue()| Check if Map has at least one key matching specified value|
| hasEntry() | Check if Map has the specified key value pair|
| equalTo(Collections.EMPTY_MAP) | Check if empty|
| allOf()| Matches if all matchers matches|
| anyOf() | Matches if any of the matchers matches|
| greaterThanOrEqualTo() | Greater than or equal to the number|
| lessThan() | Less than the number|
| lessThanOrEqualTo() | Less than or equal to the number|
| containsString() | Check if the assertion has a string|
| emptyString() | Check if the assertion has an empty string|

### Hamcrest Documentation
Hamcrest Documentation can be found here: </br>
http://hamcrest.org/JavaHamcrest/javadoc/2.2/

### Logging Cheatsheet

| Methods | Descriptions  |
| :-: | :-: |
| .all()| Log all the response |
| .body() | Shows the response body|
| .header() | Shows the header|
| .cookies() | Shows the cookies|
| .parameters() | Shows the request parameters|
| ifError() | Only show logs if there is an error|
| ifValidationFails() | Only shows log if validation fails|

```
// only show logs for both request and response if validation fails
.config(config.logConfig(LogConfig.logConfig().enableLoggingOfRequestAndResponseIfValidationFails()))

// block listing a header
.config(config.logConfig(LogConfig.logConfig().blacklistHeader("x-api-key")))
```