package com.rest;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Collections;

import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class AutomateGet {

    @Test
    public void validate_get_status_code() {
        /*
        * given statement is where you put
        * the request body, header and parameter
        *
        * when statement is where we do the action
        * we are going to take. Like get request
        *
        * then statement is the outcome or result
        * of the event. Or validate like response body,
        * status code
        * */
        given()
        .baseUri("https://api.postman.com")
        .header("x-api-key", "PMAK-607f914cb7114b004356b032-d307637aba5ca60eff96fcfb013a732ff2")
        .when()
        .get("/workspaces")
        .then()
        .log().all()
        .assertThat().statusCode(200)
        .body("workspaces.name", hasItem("My Workspace"),
                "workspaces.type", hasItem("personal"),
                "workspaces.size()", equalTo(1));
    }

    @Test
    public void extract_response(){
        Response res = given()
        .baseUri("https://api.postman.com")
        .header("x-api-key", "PMAK-607f914cb7114b004356b032-d307637aba5ca60eff96fcfb013a732ff2")
        .when()
        .get("/workspaces")
        .then()
        .assertThat()
        .statusCode(200)
        .extract()
        .response();

        /* Convert the response into a String */
        System.out.println("response - " + res.asString());
    }

    @Test
    public void extract_single_value_from_response(){
        Response res = given()
                .baseUri("https://api.postman.com")
                .header("x-api-key", "PMAK-607f914cb7114b004356b032-d307637aba5ca60eff96fcfb013a732ff2")
                .when()
                .get("/workspaces")
                .then()
                .assertThat()
                .statusCode(200)
                .extract()
                .response();

        JsonPath jsonPath = new JsonPath(res.asString());
        /* Using Json Path*/
        System.out.println("workspace name - " + jsonPath.getString("workspaces[0].name"));

        /* Using res.path*/
        System.out.println("workspace name - " + res.path("workspaces[0].name"));

        /* third option */
//        String res = given()
//                .baseUri("https://api.postman.com")
//                .header("x-api-key", "PMAK-607f914cb7114b004356b032-d307637aba5ca60eff96fcfb013a732ff2")
//                .when()
//                .get("/workspaces")
//                .then()
//                .assertThat()
//                .statusCode(200)
//                .extract()
//                .response().asString();
        //JsonPath.from(res).getString();
        //System.out.println("workspace name - " + res.path("workspaces[0].name"));

        /* Fourth Path*/
//        String name = given()
//                .baseUri("https://api.postman.com")
//                .header("x-api-key", "PMAK-607f914cb7114b004356b032-d307637aba5ca60eff96fcfb013a732ff2")
//                .when()
//                .get("/workspaces")
//                .then()
//                .assertThat()
//                .statusCode(200)
//                .extract()
//                .response().path("workspaces[0].name");
//
//        System.out.println("workspace name - " + name);
    }

    @Test
    public void hamcrest_assert_on_extracted_response(){
        String name = given()
                .baseUri("https://api.postman.com")
                .header("x-api-key", "PMAK-607f914cb7114b004356b032-d307637aba5ca60eff96fcfb013a732ff2")
                .when()
                .get("/workspaces")
                .then()
                .assertThat()
                .statusCode(200)
                .extract()
                .response().path("workspaces[0].name");

        System.out.println("workspace name - " + name);

        /* Assertion using hamcrest */
        assertThat(name, equalTo("My Workspace"));

        /* Assertion using TestNG */
        Assert.assertEquals(name, "My Workspace");
    }

    @Test
    public void validate_response_body_hamcrest_learning(){
        given()
        .baseUri("https://api.postman.com")
        .header("x-api-key", "PMAK-607f914cb7114b004356b032-d307637aba5ca60eff96fcfb013a732ff2")
        .when()
        .get("/workspaces")
        .then()
                .assertThat()
                .statusCode(200)
                .body("workspaces.name", contains("My Workspace"),
                        "workspaces.name",is(not(empty())),
                                "workspaces.name", hasSize(1),
                        "workspaces[0]", hasKey("id"),
                        "workspaces[0]", hasValue("My Workspace"),
                        "workspaces[0]", hasEntry("id", "4f4a34bd-ef2e-48a5-8ca9-052551870ea5"),
                        "workspaces[0]", not(equalTo(Collections.EMPTY_MAP)),
                        "workspaces[0].name", allOf(startsWith("My"), containsString("Workspace"))
                 );
    }
}
