package com.rest;

import io.restassured.config.LogConfig;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Collections;

import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class Logging {

    @Test
    public void request_response_logging(){
        given()
                .baseUri("https://api.postman.com")
                .header("x-api-key", "PMAK-607f914cb7114b004356b032-d307637aba5ca60eff96fcfb013a732ff2")
                .log().headers()
                .when()
                .get("/workspaces")
                .then()
                .log().body()
                .assertThat()
                .statusCode(200);
    }

    @Test
    public void log_only_if_error(){
        given()
                .baseUri("https://api.postman.com")
                .header("x-api-key", "PMAK-607f914cb7114b004356b032-d307637aba5ca60eff96fcfb013a732ff2")
                .log().all()
                .when()
                .get("/workspaces")
                .then()
                .log().ifError() // only print logs if error in response
                .assertThat()
                .statusCode(200);
    }

    @Test
    public void log_only_if_validation_fails(){
        given()
                .baseUri("https://api.postman.com")
                .header("x-api-key", "PMAK-607f914cb7114b004356b032-d307637aba5ca60eff96fcfb013a732ff2")
                .config(config.logConfig(LogConfig.logConfig().enableLoggingOfRequestAndResponseIfValidationFails()))
                //.log().ifValidationFails()
                .when()
                .get("/workspaces")
                .then()
                //.log().ifValidationFails() // only print logs if error in response
                .assertThat()
                .statusCode(200);
    }

    @Test
    public void logs_blacklist_header(){
        given()
                .baseUri("https://api.postman.com")
                .header("x-api-key", "PMAK-607f914cb7114b004356b032-d307637aba5ca60eff96fcfb013a732ff2")
                .config(config.logConfig(LogConfig.logConfig().blacklistHeader("x-api-key")))
                .log().all()
                .when()
                .get("/workspaces")
                .then()
                .assertThat()
                .statusCode(200);
    }
}
